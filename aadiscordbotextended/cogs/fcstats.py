"""
"Time" cog for discordbot - https://github.com/pvyParts/allianceauth-discordbot
"""

import logging
from datetime import datetime
from datetime import timedelta

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands
from discord.commands import Option

from django.utils import timezone
from django.contrib.auth.models import User
from aadiscordbotextended.models import FcStatsCog
from afat.models import FatLink

from django.conf import settings

logger = logging.getLogger(__name__)


class FcStats(commands.Cog):
    """
    A series of Time tools
    """

    def __init__(self, bot):
        self.bot = bot

    def build_stats(self, timeframe):
        
        user_argument = timeframe

        fc_roles = FcStatsCog.objects.values_list("group__name", flat=True).distinct()

        fcs = User.objects.filter(groups__name__in=fc_roles)

        stats = dict()

        description = str()

        footer = "Fat links for roles: {0}".format(", ".join(fc_roles))

        if user_argument:

            title = "Fat link statistics for the past {0} days".format(user_argument)

        else:

            title = "Fat link statistics for all time"

        for fc in fcs:

            fat_links = FatLink.objects.filter(creator=fc)

            if user_argument:

                timeframe = timezone.now() - timedelta(days=int(user_argument))

                fat_links = fat_links.filter(created__gte=timeframe)

            stats[fc.profile.main_character.character_name] = fat_links.count()

        sort_fats = sorted(stats.items(), key=lambda x: x[1], reverse=True)

        for key, stats in sort_fats:

            description += f"{key}: {stats}\n"

        embed = Embed(title=title)
        embed.colour = Color.blue()
        embed.description = description
        embed.set_footer(text=footer)

        return embed

    @commands.slash_command(name="fcstats", description="List of fleets raised by Fleet Commanders", guild_ids=[int(settings.DISCORD_GUILD_ID)])
    async def fcstats_slash(
        self,
        ctx,
        timeframe: Option(
            int,
            description="Timeframe in days past, leave empty for all",
            required=False,
        ),
    ):

        # Returns FC Stats
        return await ctx.respond(embed=self.build_stats(timeframe))

def setup(bot):
    bot.add_cog(FcStats(bot))