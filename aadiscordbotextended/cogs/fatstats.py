"""
"Time" cog for discordbot - https://github.com/pvyParts/allianceauth-discordbot
"""

import logging
from datetime import datetime
from datetime import timedelta

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands
from discord.commands import Option

from discord import Member
from allianceauth.services.modules.discord.models import DiscordUser
from afat.models import Fat, FatLink

from django.utils import timezone
from django.contrib.auth.models import User
from aadiscordbotextended.models import FcStatsCog
from afat.models import FatLink
from allianceauth.authentication.models import CharacterOwnership

from django.conf import settings
from app_utils.urls import static_file_absolute_url

logger = logging.getLogger(__name__)


class FatStats(commands.Cog):
    """
    A series of Time tools
    """

    def __init__(self, bot):
        self.bot = bot

    def build_stats(self, user_tag):
        
        # Get tagged user
        user_argument = user_tag

        # Try to see if the tagged user has an active discord service. If not they are not registered on AUTH and do not have FAts either
        try:

            discord_user = DiscordUser.objects.get(uid=user_tag.id)
            
        except DiscordUser.DoesNotExist:

            embed = Embed(title="Fat statistics not available")
            embed.colour = Color.red()
            embed.description = "Discord user {0} is not registered on AUTH discord service. Fat Statistics can't be displayed for the user.".format(user_tag)
            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )

            return embed

        else:

            auth_user = discord_user.user

            characters = CharacterOwnership.objects.filter(user=auth_user).values_list("character", flat=True)

            fat_links = Fat.objects.filter(character__in=characters).values_list("fatlink", flat=True)

            grip = FatLink.objects.filter(fleet__icontains="grip", id__in=fat_links)

            if grip:
                grip_completed = True
            else:
                grip_completed = False

            embed = Embed(title="Fat statistics results:")
            embed.colour = Color.green()
            embed.add_field(name="User:", value="<@!{0}>".format(user_tag.id),inline=False)
            embed.add_field(name="Fat count:", value="{0}".format(fat_links.count()),inline=True)
            embed.add_field(name="GRIP completed:", value=grip_completed,inline=True)

        return embed
    
    @commands.has_role("Member")
    @commands.slash_command(name="fats", description="User fleet participation statistics", guild_ids=[int(settings.DISCORD_GUILD_ID)])
    async def fats_slash(
        self,
        ctx,
        user_tag: Option(Member, "User", required=True)
    ):

        # Returns FC Stats
        return await ctx.respond(embed=self.build_stats(user_tag))

def setup(bot):
    bot.add_cog(FatStats(bot))