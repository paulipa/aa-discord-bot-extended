"""
"Time" cog for discordbot - https://github.com/pvyParts/allianceauth-discordbot
"""

import logging
from datetime import datetime
from datetime import timedelta

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands
from discord.commands import Option

from app_utils.urls import static_file_absolute_url
from aadiscordbotextended.models import HowToCog
from aadiscordbotextended.models import Embed as EmbedLines

from django.conf import settings

logger = logging.getLogger(__name__)


class Howto(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    actions = HowToCog.objects.all().values_list("title", flat=True)

    def build_guide(self, action):

        user_argument = action

        howto = HowToCog.objects.get(title=user_argument)

        embed_lines = EmbedLines.objects.all().filter(
                    howtocog__title=user_argument
                )

        embed = Embed(title="How to: {}".format(user_argument))

        embed.colour = Color.blue()

        embed.description = howto.description

        embed.set_author(name=howto.author)

        embed.url = howto.url

        if howto.image:
            embed.set_image(url=howto.image)

        embed.set_thumbnail(
                    url=static_file_absolute_url("aadiscordbotextended/help.png")
                )

        for x in embed_lines:
            embed.add_field(
                name=x.title,
                value=x.description,
                inline=False,
            )

        return embed

    @commands.slash_command(name="howto", description="Returns simplified guides for tasks", guild_ids=[int(settings.DISCORD_GUILD_ID)])
    async def howto_slash(
        self,
        ctx,
        action: Option(
            str,
            choices=actions,
            description="Select task type",
            required=True,
        ),
    ):

        # Returns FC Stats
        return await ctx.respond(embed=self.build_guide(action))

def setup(bot):
    bot.add_cog(Howto(bot))