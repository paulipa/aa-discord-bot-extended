"""
"Time" cog for discordbot - https://github.com/pvyParts/allianceauth-discordbot
"""

import logging
from datetime import datetime
from datetime import timedelta

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands
from discord.commands import Option

from discord import Member
from allianceauth.services.modules.discord.models import DiscordUser
from app_utils.urls import static_file_absolute_url


from django.conf import settings

logger = logging.getLogger(__name__)


class Status(commands.Cog):
    """
    A series of Time tools
    """

    def __init__(self, bot):
        self.bot = bot

    def build_status(self, ctx, user):
        
        if not user:
            id = ctx.author.id
            name = ctx.author
        else:
            id = user.id
            name = user

        try:
            discord_user = DiscordUser.objects.get(uid=id)

            if discord_user:
                discord_active = True

            embed = Embed(title="Account synchronized with AUTH")
            embed.colour = Color.green()
            embed.add_field(name="Synced status", value=discord_active, inline=True)
            embed.add_field(name="User", value=name, inline=True)
            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )
            embed.description = (
                "This account is linked to a LinkNet AUTH user correctly."
            )

            return embed

        except Exception:
            discord_active = False

            embed = Embed(title="Account not synchronized with AUTH")
            embed.colour = Color.red()
            embed.add_field(name="Synced status", value=discord_active, inline=True)
            embed.add_field(name="User", value=name, inline=True)
            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )
            embed.description = "The selected account is not synced to LinkNet AUTH. See `/howto discord` and /howto fixdiscod` for more help."

            return embed



    @commands.slash_command(name="status", description="Returns user discord service status", guild_ids=[int(settings.DISCORD_GUILD_ID)])
    async def status_slash(
        self,
        ctx,
        user: Option(Member, description="The user to check the status for. Leave empty for self.", required=False)
    ):

        # Returns FC Stats
        return await ctx.respond(embed=self.build_status(ctx, user))

def setup(bot):
    bot.add_cog(Status(bot))