from django import forms

from aadiscordbotextended.models import Embed, HowToCog


class HowToCogAdminForm(forms.ModelForm):
    class Meta:
        model = HowToCog
        fields = "__all__"


class EmbedAdminForm(forms.ModelForm):
    class Meta:
        model = Embed
        fields = "__all__"
