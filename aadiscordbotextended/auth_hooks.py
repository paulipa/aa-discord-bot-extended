from allianceauth import hooks


@hooks.register("discord_cogs_hook")
def register_cogs():
    return [
        "aadiscordbotextended.cogs.howto",
        "aadiscordbotextended.cogs.status",
        "aadiscordbotextended.cogs.hyperlink_filter",
        "aadiscordbotextended.cogs.votekick",
        "aadiscordbotextended.cogs.fcstats",
        "aadiscordbotextended.cogs.fatstats",
    ]
